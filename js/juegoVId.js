const filas = 60;
const columnas = 60;
let tablero = inicializarTablero();
let dibujando = false;

function inicializarTablero() {
    const nuevoTablero = [];
    for (let i = 0; i < filas; i++) {
        nuevoTablero[i] = [];
        for (let j = 0; j < columnas; j++) {
            nuevoTablero[i][j] = 0;
        }
    }
    return nuevoTablero;
}

function dibujarTablero() {
    const tableroDiv = document.getElementById('tablero');
    tableroDiv.innerHTML = '';

    for (let i = 0; i < filas; i++) {
        for (let j = 0; j < columnas; j++) {
            const cellDiv = document.createElement('div');
            cellDiv.classList.add('cell');
            cellDiv.style.backgroundColor = tablero[i][j] ? 'black' : 'white';
            cellDiv.addEventListener('mouseover', () => dibujando && dibujarBacteriaManual(i, j));
            tableroDiv.appendChild(cellDiv);
        }
    }
}

function dibujarBacteriaManual(fila, columna) {
    tablero[fila][columna] = 1;
    dibujarTablero();
}

function dibujarBacteria(event) {
    dibujando = true;
    document.addEventListener('mouseup', () => dibujando = false);
    document.addEventListener('mousemove', (e) => {
        const boundingRect = document.getElementById('tablero').getBoundingClientRect();
        const fila = Math.floor((e.clientY - boundingRect.top) / 10);
        const columna = Math.floor((e.clientX - boundingRect.left) / 10);

        if (fila >= 0 && fila < filas && columna >= 0 && columna < columnas) {
            tablero[fila][columna] = 1;
            dibujarTablero();
        }
    });
}

function iniciarJuego() {
    const generaciones = parseInt(document.getElementById('generaciones').value);
    const poblacion = parseInt(document.getElementById('poblacion').value);

    // Generar población inicial aleatoria
    for (let i = 0; i < poblacion; i++) {
        const randomFila = Math.floor(Math.random() * filas);
        const randomColumna = Math.floor(Math.random() * columnas);
        tablero[randomFila][randomColumna] = 1;
    }

    dibujarTablero();

    // Iniciar la simulación
    let generacionActual = 0;
    const intervalId = setInterval(() => {
        if (generacionActual < generaciones) {
            siguienteGeneracion();
            generacionActual++;
        } else {
            pausarJuego(intervalId);
        }
    }, 100);
}

function pausarJuego(intervalId) {
    clearInterval(intervalId);
}

function generarAleatorio() {
    for (let i = 0; i < filas; i++) {
        for (let j = 0; j < columnas; j++) {
            tablero[i][j] = Math.random() > 0.5 ? 1 : 0;
        }
    }
    dibujarTablero();
}

function limpiarTablero() {
    tablero = inicializarTablero();
    dibujarTablero();
}